﻿using System.Web;
using System.Web.Optimization;

namespace EmbraceSalon
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery.js",
                        "~/Scripts/bootstrap.min.js",
                        "~/Scripts/placeholders.js",
                        "~/Scripts/jquery.magnific-popup.min.js",
                        "~/Scripts/owl.carousel.min.js",
                        "~/Scripts/respond.min.js",
                        "~/Scripts/html5shiv.js",
                        "~/Scripts/main.js",
                        "~/Scripts/jquery.themepunch.tools.min.js",
                        "~/Scripts/jquery.themepunch.revolution.min.js",
                        "~/Scripts/waypoints.min.js",
                        "~/Scripts/jquery.countTo.js"));




            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/font-awesome.min.css",
                      "~/Content/magnific-popup.css",
                      "~/Content/owl.carousel.css",
                      "~/Content/settings.css",
                      "~/Content/styles/style.css",
                      "~/Content/styles/skin-lblue.css",
                      "~/Content/custom.css"));
        }
    }
}
