﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EmbraceSalon.Controllers
{
    public class SalonServicesController : Controller
    {
        // GET: SalonServices
        public ActionResult Index()
        {
           return RedirectToAction("Services");
        }

        public ActionResult Services()
        {
            return View();
        }

        public ActionResult EyeLashes()
        {
            return View();
        }

        public ActionResult BridalPackages()
        {
            return View();
        }
    }
}